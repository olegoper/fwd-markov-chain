#pragma once

#include <stddef.h>
#include <string>
#include <vector>

namespace textgen {

class TextParser {
public:
    TextParser(const std::string& text);

    bool hasNext() {
        return lastIndex_ < words_.size();
    }

    const std::string& next() {
        return words_.at(lastIndex_++);
    }

    static std::string normalizeWord(const std::string& w);

private:
    std::vector<std::string> splitWords(const std::string& text);

    size_t lastIndex_ = 0;
    std::vector<std::string> words_;
};

} // namespace textgen
