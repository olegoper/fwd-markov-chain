#include "dictionary.h"

#include <sstream>
#include <stdexcept>

namespace textgen {

int32_t Dictionary::get(const std::string& word) {
    const auto it = dict_.find(word);
    if (it != dict_.end()) {
        return it->second;
    }
    throw std::runtime_error("Word not in the dictionary: " + word);
}

int32_t Dictionary::insert(const std::string& word) {
    const auto it = dict_.find(word);
    if (it != dict_.end()) {
        return it->second;
    }
    dict_[word] = nextId_;
    return nextId_++;
}

void Dictionary::serialize(std::ostream& to) {
    to << dict_.size() << std::endl;
    for (const auto& it : dict_) {
        serializeSingle(to, it.first, it.second);
    }
}

std::unordered_map<int32_t, std::string> Dictionary::buildReverseDict() {
    std::unordered_map<int32_t, std::string> res;
    for (auto& it : dict_) {
        res[it.second] = it.first;
    }
    return res;
}

void Dictionary::deserialize(std::istream& from) {
    int len = deserializeInt(from);

    std::string line;
    while (len > 0 && std::getline(from, line)) {
        deserializeSingle(line);
        len--;
    }
}

int Dictionary::deserializeInt(std::istream& from) {
    int value;
    std::string line;
    if (!std::getline(from, line)) {
        throw std::runtime_error("bad dict file");
    } else {
        std::istringstream iss(line);
        iss >> value;
    }
    return value;
}

void Dictionary::serializeSingle(std::ostream& to, const std::string& word, int32_t id) {
    to << id << " ";
    to << word;
    to << std::endl;
}

void Dictionary::deserializeSingle(const std::string& line) {
    std::istringstream iss(line);
    int32_t id;
    iss >> id;
    std::string word;
    iss >> word;
    if (!iss) {
        throw std::runtime_error("bad dict file");
    }
    dict_[word] = id;
}

} // namespace textgen
