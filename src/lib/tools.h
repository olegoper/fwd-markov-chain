#pragma once

#include <stddef.h>
#include <stdint.h>
#include <deque>
#include <random>
#include <unordered_map>
#include <utility>

namespace textgen {

class DequeHasher {
public:
    size_t operator() (const std::deque<int32_t>& c) const {
        size_t seed = c.size();
        for(auto& i : c) {
            seed ^= i + 0x9e3779b9 + (seed << 6) + (seed >> 2);
        }
        return seed;
    }
};

class RandomSelector {
public:
    using DistributionRange = std::vector<std::pair<double, int32_t>>;

    RandomSelector();
    static DistributionRange makeRange(const std::unordered_map<int32_t, int>& stats);
    int32_t pickAtRandom(DistributionRange& range);

private:
    double nextRandom();

    std::random_device randomDevice_;
    std::mt19937 randEngine_;
};

} //namespace textgen

