#include "markov_chain.h"

#include <algorithm>
#include <sstream>
#include <stdexcept>

namespace textgen {

MarkovChainModel::MarkovChainModel(
    int size,
    const std::unordered_map<std::deque<int32_t>, RandomSelector::DistributionRange, DequeHasher>& model):
         size_(size),
         model_(model)
{
    for (auto& it : model_) {
        if (it.first.size() != size_) {
            throw std::runtime_error("bad model data size");
        }
    }
}

void MarkovChainModel::addData(const std::deque<int32_t>& state, const RandomSelector::DistributionRange& next) {
    if (state.size() != size_) {
        throw std::runtime_error("bad model data size");
    }
    model_[state] = next;
}

void MarkovChainModel::serialize(std::ostream& to) {
    to << model_.size() << std::endl;
    to << size_ << std::endl;
    for (const auto& it : model_) {
        serializeSingle(to, it.first, it.second);
    }
}

bool MarkovChainModel::getNext(const std::deque<int32_t>& state, int32_t& next) {
    const auto it = model_.find(state);
    if (it == model_.end()) {
        return false;
    }
    next = randomSelector_.pickAtRandom(it->second);
    return true;
}

void MarkovChainModel::deserialize(std::istream& from) {
    int len = deserializeInt(from);
    size_ = deserializeInt(from);

    std::string line;
    while (len > 0 && std::getline(from, line)) {
        deserializeSingle(size_, line);
        len--;
    }
}

int MarkovChainModel::deserializeInt(std::istream& from) {
    int value;
    std::string line;
    if (!std::getline(from, line)) {
        throw std::runtime_error("bad model file");
    } else {
        std::istringstream iss(line);
        iss >> value;
    }
    return value;
}

void MarkovChainModel::serializeDistributionRange(std::ostream& to, const RandomSelector::DistributionRange& value) {
    to << "     " << value.size() << " ";
    for (const auto& it : value) {
        to << it.first << " " << it.second << " ";
    }
}

RandomSelector::DistributionRange MarkovChainModel::deserializeDistributionRange(std::istream& from) {
    RandomSelector::DistributionRange res;
    int size;
    from >> size;
    for (int i = 0; i < size; ++i) {
        double prob;
        int32_t word;

        from >> prob;
        from >> word;
        res.emplace_back(prob, word);
    }

    return res;
}

void MarkovChainModel::serializeSingle(std::ostream& to, const std::deque<int32_t>& state, const RandomSelector::DistributionRange& value) {
    for (auto it : state) {
        to << it << " ";
    }
    serializeDistributionRange(to, value);
    to << std::endl;
}

void MarkovChainModel::deserializeSingle(int size, const std::string& line) {
    std::istringstream iss(line);
    std::deque<int32_t> state;
    state.resize(size);
    for (int i = 0; i < size; ++i) {
        iss >> state[i];
    }
    RandomSelector::DistributionRange next = deserializeDistributionRange(iss);

    if (!iss) {
        throw std::runtime_error("bad model file");
    }
    model_[state] = next;
}

MarkovChainModelBuilder::MarkovChainModelBuilder(int size): size_(size) {
    if (size_ < 1) {
        throw std::runtime_error("Model size should be > 0");
    }
}

void MarkovChainModelBuilder::train(int32_t nextWord) {
    if (static_cast<int>(state_.size()) == size_) {
        model_[state_][nextWord]++;
        state_.pop_front();
    }
    state_.push_back(nextWord);
}

std::unique_ptr<MarkovChainModel> MarkovChainModelBuilder::build() {
    std::unordered_map<std::deque<int32_t>, RandomSelector::DistributionRange, DequeHasher> model;
    for (const auto& it : model_) {
        model[it.first] = RandomSelector::makeRange(it.second);
    }

    return std::make_unique<MarkovChainModel>(size_, model);
}

MarkovChainGenerator::MarkovChainGenerator(std::unique_ptr<MarkovChainModel>&& model, const std::deque<int32_t> state):
    state_(state)
{
    if (state_.empty()) {
        throw std::runtime_error("Empty state is invalid");
    }
    if (model->getModelSize() != state_.size()) {
        throw std::runtime_error("State size doesn't match model size");
    }
    model_ = std::move(model);
}

bool MarkovChainGenerator::generate(int32_t& next) {
    if (model_->getNext(state_, next)) {
        state_.pop_front();
        state_.push_back(next);
        return true;
    }
    return false;
}

} // namespace textgen
