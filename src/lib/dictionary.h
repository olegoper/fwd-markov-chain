#pragma once

#include <stdint.h>
#include <iostream>
#include <string>
#include <unordered_map>

namespace textgen {

class Dictionary {
public:
    Dictionary(std::istream& from) {
        deserialize(from);
    }

    Dictionary() = default;

    int32_t get(const std::string& word);

    int32_t insert(const std::string& word);

    void serialize(std::ostream& to);

    std::unordered_map<int32_t, std::string> buildReverseDict();

private:
    void deserialize(std::istream& from);

    int deserializeInt(std::istream& from);

    void serializeSingle(std::ostream& to, const std::string& word, int32_t id);

    void deserializeSingle(const std::string& line);

    std::unordered_map<std::string, int32_t> dict_;
    int32_t nextId_ = 0;
};

} // namespace textgen
