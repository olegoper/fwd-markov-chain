#include <lib/dictionary.h>
#include <lib/markov_chain.h>
#include <lib/text_parser.h>

#include <boost/filesystem.hpp>
#include <boost/locale.hpp>
#include <boost/program_options.hpp>

#include <deque>
#include <exception>
#include <fstream>
#include <iostream>
#include <locale>
#include <memory>
#include <stdexcept>
#include <string>

namespace po = boost::program_options;

void generateChain(
    int N,
    const std::vector<std::string>& words,
    const boost::filesystem::path& modelFile)
{
    if (words.size() != N) {
        throw std::runtime_error("Number of words should be equal to model size");
    }
    std::ifstream ifs(modelFile.string());
    auto model = std::make_unique<textgen::MarkovChainModel>(ifs);
    auto dict = std::make_unique<textgen::Dictionary>(ifs);

    if (model->getModelSize() != N) {
        throw std::runtime_error("Model size from file and input doesn't match");
    }
    std::deque<int32_t> state;
    for (const auto& word: words) {
        const std::string& normalizedWord = textgen::TextParser::normalizeWord(word);
        if (normalizedWord.empty()) {
            throw std::runtime_error("Bad input word: " + word);
        }
        state.push_back(dict->get(normalizedWord));
    }

    const auto reverseDict = dict->buildReverseDict();
    dict.reset();
    textgen::MarkovChainGenerator generator(std::move(model), state);
    int32_t next;
    while (generator.generate(next)) {
        std::cout << reverseDict.at(next) << std::endl;
    }
}

po::variables_map parseProgramOptions(int ac, char ** av) {
    po::options_description desc("\nAllowed options");

    desc.add_options()("model-size,m", po::value<int>()->required(), "Markov model parameter");
    desc.add_options()("model-file,f", po::value<boost::filesystem::path>()->required(), "path to model file to save");

    desc.add_options()("word", po::value<std::vector<std::string>>()->multitoken(), "words to start with");

    po::variables_map vm;
    po::store(parse_command_line(ac, av, desc), vm);

    if (vm.empty()) {
        std::ostringstream outStr;
        desc.print(outStr);
        throw std::runtime_error(std::string("Bad program options") + outStr.str());
    }

    int modelSize = vm["model-size"].as<int>();
    if (!vm.count("word") || modelSize < 1) {
        std::ostringstream outStr;
        desc.print(outStr);
        throw std::runtime_error(std::string("Number of words should equal to model size and be > 1") + outStr.str());
    }

    notify(vm);
    return vm;
}

int main(int ac, char ** av) {
    try {
		boost::locale::generator gen;
		std::locale loc = gen("");
		std::locale::global(loc);
        auto opts = parseProgramOptions(ac, av);
        generateChain(
            opts["model-size"].as<int>(),
            opts["word"].as<std::vector<std::string>>(),
            opts["model-file"].as<boost::filesystem::path>());
    } catch (std::exception& e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return 1;
    }
}

