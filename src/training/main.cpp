#include "text_download.h"

#include <lib/dictionary.h>
#include <lib/markov_chain.h>
#include <lib/text_parser.h>

#include <boost/filesystem.hpp>
#include <boost/locale.hpp>
#include <boost/program_options.hpp>

#include <exception>
#include <fstream>
#include <iostream>
#include <locale>
#include <stdexcept>
#include <string>

namespace po = boost::program_options;

po::variables_map parseProgramOptions(int ac, char ** av) {
    po::options_description desc("\nAllowed options");

    desc.add_options()("model-size,m", po::value<int>()->required(), "Markov model parameter");
    desc.add_options()("model-file,f", po::value<boost::filesystem::path>()->required(), "path to model file to save");

    desc.add_options()("url", po::value<std::vector<std::string>>()->multitoken(), "urls to download");

    po::variables_map vm;
    po::store(parse_command_line(ac, av, desc), vm);

    if (vm.empty()) {
        std::ostringstream outStr;
        desc.print(outStr);
        throw std::runtime_error(std::string("Bad program options") + outStr.str());
    }

    if (!vm.count("url")) {
        std::ostringstream outStr;
        desc.print(outStr);
        throw std::runtime_error(std::string("Urls required") + outStr.str());
    }

    notify(vm);
    return vm;
}

void buildModel(
    int N,
    const std::vector<std::string>& urls,
    const boost::filesystem::path& modelFile)
{
    textgen::Dictionary dict;
    textgen::MarkovChainModelBuilder modelBuilder(N);

    for (const auto& url: urls) {
        modelBuilder.resetState();
        std::string text = textgen::download_text_from_url(url);
        textgen::TextParser parser(text);
        while (parser.hasNext()) {
            auto word = parser.next();
            int32_t id = dict.insert(word);
            modelBuilder.train(id);
        }
    }
    std::unique_ptr<textgen::MarkovChainModel> model = modelBuilder.build();

    std::ofstream ofs(modelFile.string());
    model->serialize(ofs);
    dict.serialize(ofs);
}

int main(int ac, char ** av) {
    try {
        boost::locale::generator gen;
        std::locale loc = gen("");
        std::locale::global(loc);

        auto opts = parseProgramOptions(ac, av);
        buildModel(
            opts["model-size"].as<int>(),
            opts["url"].as<std::vector<std::string>>(),
            opts["model-file"].as<boost::filesystem::path>());
    } catch (std::exception& e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return 1;
    }
}

