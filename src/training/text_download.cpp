#include "text_download.h"

#include <cstdio>
#include <array>
#include <iostream>
#include <memory>
#include <stdexcept>

namespace {

std::string exec(const std::string& cmd) {
    std::array<char, 1024> buffer;
    std::string result;
    std::shared_ptr<FILE> pipe(popen(cmd.c_str(), "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (!feof(pipe.get())) {
        if (fgets(buffer.data(), 1024, pipe.get()) != nullptr)
            result += buffer.data();
    }
    return result;
}

} // namespace

namespace textgen {

std::string download_text_from_url(const std::string& url) {
    return exec("curl --silent " + url);
}

} // namespace textgen
