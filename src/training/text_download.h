#pragma once

#include <string>

namespace textgen {

std::string download_text_from_url(const std::string& url);

} // namespace textgen
