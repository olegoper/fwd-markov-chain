cmake_minimum_required(VERSION 3.1.0 FATAL_ERROR)

project(mark)

# Compiler options
add_compile_options(-std=c++14)

# External libraries
set(Boost_USE_STATIC_LIBS ON)
find_package(Boost REQUIRED
    locale
    system
    filesystem
    program_options
)

# Includes
include_directories(${Boost_INCLUDE_DIRS})
include_directories(src)

# Targets
set (COMMON_LIB common)
set (TRAINING_APP training)
set (GENERATOR_APP generator)

# Common library
set (LIB_SOURCES
    src/lib/dictionary.cpp
    src/lib/markov_chain.cpp
    src/lib/tools.cpp
    src/lib/text_parser.cpp
)

add_library(${COMMON_LIB} ${LIB_SOURCES})

# Training app
set (TRAINING_SOURCES
    src/training/text_download.cpp
    src/training/main.cpp
)

# Generator app
set (GENERATOR_SOURCES
    src/generator/main.cpp
)

add_executable(${TRAINING_APP} ${TRAINING_SOURCES})
add_executable(${GENERATOR_APP} ${GENERATOR_SOURCES})

target_link_libraries(${TRAINING_APP} ${COMMON_LIB} ${Boost_LIBRARIES})
target_link_libraries(${GENERATOR_APP} ${COMMON_LIB} ${Boost_LIBRARIES})
